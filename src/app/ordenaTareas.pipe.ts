import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'ordenatareas',
  pure: false
})
export class OrdenaTareasPipe implements PipeTransform{
  transform(value: any, ...args: any[]): any {
    if(!value || value.length <2 ) return value;
    let propiedad=args[0];
    let asc= args[1];
    let auxa:any;
    let auxb:any;

    value.sort((a:any,b:any)=>{
      if (propiedad){
        if (asc) {
          auxa = a.prioridad;
          auxb = b.prioridad;
        } else {
          auxb = a.prioridad;
          auxa = b.prioridad;
        }
      }else{
        if (asc){
          auxa=a.action.toLowerCase();
          auxb=b.action.toLowerCase();
        } else {
          auxb=a.action.toLowerCase();
          auxa=b.action.toLowerCase();
        }
      }
      if(auxa<auxb) return -1;
      else if (auxa>auxb) return 1;
      else return 0;
    });
    return value;
  }

}
