import {Component, OnDestroy} from '@angular/core';
import {TodoService} from './TodoService';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy{

  title = "Lista de DAW's";
  model = {
    user: 'Daw',
    items: [ ]
  };
  suscripcion:Subscription;
  mostrarTodas=true;
  constructor(private todoService:TodoService) {
   // this.ordenaTareas();
    //this.model.items =todoService.getItems();
    this.suscripcion=todoService.getItems().subscribe((data:any)=>this.model.items=data);
  }

  TnIncompletas(){
    let count=0;
    if (this.model.items)
      this.model.items.forEach((item,index)=>!item.done? count++:true);
    return count;
  }

  addItem(tarea){
    //console.log(tarea);
    let nuevoId=Math.random().toString(36).substr(2,10);
    this.model.items.push({id: nuevoId, action: tarea, done: false, prioridad: 4});
   // this.ordenaTareas();
  }

  nuevaPrioridad($event: any,id) {
    //console.log(i);
    let i= this.model.items.findIndex((item:any)=>item.id==id, id);
    this.model.items[i].prioridad=$event;
  }

/*  ordenaTareas(){
    this.model.items.sort((a:any,b:any)=>{
      if(a.action.toLowerCase()<b.action.toLowerCase()) return -1;
      else if (a.action.toLowerCase()>b.action.toLowerCase()) return 1;
      else return 0;
    })
  }*/
  porPrioridad: boolean=true;
  asc:boolean=true;
  eliminaTarea(id: any) {
    let i= this.model.items.findIndex((item:any)=>item.id==id, id);
    this.model.items.splice(i,1);
  }

  setCriterios(b: boolean) {
    this.porPrioridad=b;
    this.asc=!this.asc;
  }

  ngOnDestroy(): void {
    this.suscripcion.unsubscribe();
  }
}
