import {Component, OnDestroy, OnInit} from '@angular/core';
import {fromEvent, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map, switchMap} from 'rxjs/operators';
import {fromPromise} from 'rxjs/internal-compatibility';
declare var $:any;

@Component({
  selector: 'reactive',
  template: `
    <input type="text" class="form-control" id="search" placeholder="Search ...">
  `
})
export class ReactiveComponent implements OnInit,OnDestroy{
  keyups:any;
  flickerApi="https://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?";
  suscripcion:Subscription;

  constructor() {}

  ngOnInit(){
    this.keyups = fromEvent($('#search'),'keyup')
      .pipe(
        map((e:any)=>e.target.value),
        filter(text=> text.length>3),
        debounceTime(400),
        distinctUntilChanged(),
        switchMap(searchTErm =>{
          let promesa=$.getJSON(this.flickerApi,{
            tags:searchTErm,
            tagmode: "all",
            format: "json"
          });
          let observable = fromPromise(promesa);
          return observable;
        })
      );

    this.suscripcion=this.keyups.subscribe(
      (data:any)=>console.log(data),
      (error:any)=>console.log("error",error),
      ()=>console.log("completado")
    );

  }

  ngOnDestroy(): void {
    this.suscripcion.unsubscribe();
  }
}
